package seminar03

import cats.instances.all._
import cats.laws.discipline.FunctorTests
import org.scalatest.prop.Configuration
import ru.tinkoff.fintech.lectures.Bin

class BinFunctorTest
    extends AnyFunSuite
    with Configuration
    with FunSuiteDiscipline {

  // identity:    fa.map(identity) <-> fa
  // composition: fa.map(f).map(g) <-> fa.map(f.andThen(g))
  checkAll("Bin.FunctorLaws", FunctorTests[Bin].functor[Int, Int, String])

}
