package seminar03

import cats.implicits._
import cats.laws.discipline.ApplicativeTests
import cats.{Applicative, Apply}
import org.scalatest.prop.Configuration
import ru.tinkoff.fintech.lectures.Bin
import ru.tinkoff.fintech.lectures.lecture04.BinApply

// unlawful implementation!
trait BinApplicative extends Applicative[Bin] {
  val binApply: Apply[Bin] = new BinApply {}

  override def pure[A](x: A): Bin[A] = Bin.one(x)

  override def ap[A, B](ff: Bin[A => B])(fa: Bin[A]): Bin[B] =
    binApply.ap(ff)(fa)
}

class BinApplicativeTest
    extends AnyFunSuite
    with Configuration
    with FunSuiteDiscipline {
  implicit val F = new BinApplicative {}

  // identity:      F.pure(identity).ap(fa) <-> fa
  checkAll("Bin.ApplicativeLaws", ApplicativeTests[Bin].apply[Int, Int, String])

}

