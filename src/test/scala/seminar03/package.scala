import org.scalacheck.{Arbitrary, Gen}
import ru.tinkoff.fintech.lectures.Bin

package object seminar03 {
  def binGen[A](maxDepth: Int)(implicit elemGen: Gen[A]): Gen[Bin[A]] = {
    def go(depth: Int): Gen[Bin[A]] = {
      if (depth == 1) Gen.const(Bin.empty[A])
      else {
        Gen.oneOf[Bin[A]](
          for {
            left <- go(depth - 1)
            right <- go(depth - 1)
            branch <- elemGen.map(elem => Bin.Branch(elem, left, right))
          } yield branch,
          Bin.empty[A]
        )
      }
    }

    go(maxDepth)
  }

  implicit def arbitratyBin[A](implicit arbA: Arbitrary[A]): Arbitrary[Bin[A]] =
    Arbitrary(binGen(3)(arbA.arbitrary))
}
