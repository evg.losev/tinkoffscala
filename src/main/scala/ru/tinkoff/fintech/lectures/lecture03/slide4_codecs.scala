package ru.tinkoff.fintech.lectures.lecture03

import java.time.Instant
import java.util.UUID

import io.circe.{Decoder, Json}
import io.circe.derivation.annotations.JsonCodec

object MyInstances {
  implicit def binDecoder[A: Decoder]: Decoder[Bin[A]] = Decoder.instance { cur =>
    if (cur.value == Json.Null) Right(Bin.Leaf)
    else {
      val first = cur.downArray
      for {
        elem   <- first.as[A]
        second = first.right
        left   <- second.as[Bin[A]]
        right  <- second.right.as[Bin[A]]
      } yield Bin.Branch(elem, left, right)
    }
  }
}
object slide4_codecs extends App {
  import io.circe._
  import io.circe.syntax._
  import io.circe.parser._

//  println(List(1, 2, 3, 4).asJson.spaces2)
//  println(Map("now" -> Instant.now(), "zero" -> Instant.ofEpochMilli(0)).asJson.spaces2)

  @JsonCodec
  case class User(
      name: String,
      id: UUID,
      roles: List[String]
  )

  val user = User(
    name = "Oleg",
    id = UUID.fromString("d055770e-ffe1-11e9-b10a-0f76dc90044b"),
    roles = List("teacher", "developer")
  )

//  println(user.asJson.spaces2)

  implicit def binEncoder[A: Encoder]: Encoder[Bin[A]] = Encoder.instance {
    case Bin.Leaf                   => Json.Null
    case Bin.Branch(a, left, right) => Json.arr(a.asJson, left.asJson, right.asJson)
  }

  val exampleJson = Bin.example.asJson.spaces2
//  println(exampleJson)

  val userJson = s"""{"name": "Tanya", "roles" : ["mentor", "tester"], "id": "d055770e-ffe1-11e9-b10a-0f76dc90044b"} """

//  println(decode[User](userJson))

  import MyInstances._
  println(decode[Bin[String]](exampleJson))
  println(decode[Bin[String]](exampleJson) == Right(Bin.example))

}
