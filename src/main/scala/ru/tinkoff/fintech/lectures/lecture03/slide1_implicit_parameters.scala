package ru.tinkoff.fintech.lectures.lecture03

import scala.io.StdIn

case class HelloContext(
    greetingOne: String = "Hello",
    greetingMany: String = "Greetings to",
    greetingNone: String = "Hi",
    separator: String = ", ",
    lastSeparator: String = " and ",
    mark: String = "!",
    transform: String => String = identity
)

object HelloContext {
  implicit val default = HelloContext()

}

object slide1_context_parameters {
  def helloOne(name: String)(ctx: HelloContext) = {
    import ctx._
    s"$greetingOne ${transform(name)}$mark"
  }

  def helloMany(names: Seq[String], nameLast: String)(ctx: HelloContext) = {
    import ctx._
    s"$greetingMany ${names.map(transform).mkString(separator)}$lastSeparator${transform(nameLast)}$mark"
  }

  def helloNone(ctx: HelloContext) = {
    import ctx._
    s"$greetingNone$mark"
  }

  def hello(names: String*)(ctx: HelloContext) =
    names match {
      case Seq()         => helloNone(ctx)
      case Seq(name)     => helloOne(name)(ctx)
      case names :+ last => helloMany(names, last)(ctx)
    }

  def printHello(nameString: String)(ctx: HelloContext): Unit = {
    val values = nameString.split("[,\\s]\\s*").filterNot(_.matches("\\s*"))
    println(hello(values: _*)(ctx))
  }

  def go(ctx: HelloContext): Unit =
    StdIn.readLine() match {
      case "end" =>
      case names =>
        printHello(names)(ctx)
        go(ctx)
    }

  def main(args: Array[String]): Unit = {
//    println(helloNone(HelloContext()))
//    go(HelloContext(greetingNone = "Ohayo"))
  }
}

object slide1_implicit_parameters {
  def helloOne(name: String)(implicit ctx: HelloContext) = {
    import ctx._
    s"$greetingOne ${transform(name)}$mark"
  }

  def helloMany(names: Seq[String], nameLast: String)(implicit ctx: HelloContext) = {
    import ctx._
    s"$greetingMany ${names.map(transform).mkString(separator)}$lastSeparator${transform(nameLast)}$mark"
  }

  def helloNone(implicit ctx: HelloContext) = {
    import ctx._
    s"$greetingNone$mark"
  }

  def hello(names: String*)(implicit ctx: HelloContext) =
    names match {
      case Seq()         => helloNone
      case Seq(name)     => helloOne(name)
      case names :+ last => helloMany(names, last)
    }

  def printHello(nameString: String)(implicit ctx: HelloContext): Unit = {
    val values = nameString.split("[,\\s]\\s*").filterNot(_.matches("\\s*"))
    println(hello(values: _*))
  }

  def go(implicit ctx: HelloContext): Unit =
    StdIn.readLine() match {
      case "end" =>
      case names =>
        printHello(names)
        go
    }

  def main(args: Array[String]): Unit = {
    implicit val foo: Int = 2
    implicit val context =
      HelloContext.default.copy(greetingOne = "Aloha", separator = " und ", lastSeparator = " und ")
    go
  }
}
