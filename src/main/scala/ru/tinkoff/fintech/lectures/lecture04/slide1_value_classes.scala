package ru.tinkoff.fintech.lectures.lecture04

import java.util.UUID

import io.circe.{Codec, Decoder, Encoder}

object slide1_value_classes extends App {
  class MyInt(val inner: Int) extends AnyVal

  case class Foo(xxx: MyInt, s: String)

//  println(classOf[Foo].getDeclaredFields.foreach(println))

  class Bar {
    def sum(x: MyInt, y: MyInt): MyInt = new MyInt(x.inner + y.inner)
  }

  classOf[Bar].getDeclaredMethods.foreach(println)

//  class Outer {
//    class Inner(val inner: Int) extends AnyVal
//  }
//  type Top = Any
//  implicitly[AnyRef <:< Any]
//  implicitly[AnyVal <:< Any]

  trait Area
  case class Measure[unit](v: Double) extends AnyVal
  case class PersonId(value: UUID)    extends AnyVal

//  implicit val personIdEncoder: Encoder[PersonId] = Encoder[UUID].contramap(_.value)
//  implicit val personIdDecoder: Decoder[PersonId]  = Decoder[UUID].map(PersonId(_))

  val area = Measure[Area](12.3)
}
