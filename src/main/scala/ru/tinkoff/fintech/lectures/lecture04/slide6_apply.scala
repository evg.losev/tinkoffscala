package ru.tinkoff.fintech.lectures.lecture04

import cats.data.ZipList
import cats.{Applicative, Apply, Eval}
import cats.implicits._
import ru.tinkoff.fintech.lectures.Bin

object slide6_apply extends App {
//  println(List[Int => Int](_ + 1, _ * 2) ap List(1, 2))
//  println((ZipList[Int => Int](List(_ + 1, _ * 2, _ / 3)) ap ZipList(List(1, 2))).value)

  println((Bin.example, Bin.example.map(_.length)).tupled)
  println((Bin.example, (Bin.Leaf: Bin[Int])).tupled)

//  println((Bin.example, Bin.example, Bin.example, Bin.example).mapN((a, b, c, d) => a + b + c + d))
}

trait BinApply extends BinFunctor with Apply[Bin] {
  def ap[A, B](ff: Bin[A => B])(fa: Bin[A]): Bin[B] = map2(ff, fa)(_ apply _)

  override def map2[A, B, Z](fa: Bin[A], fb: Bin[B])(f: (A, B) => Z): Bin[Z] =
    map2Eval(fa, Eval.now(fb))(f).value

  override def map2Eval[A, B, Z](fa: Bin[A], fb: Eval[Bin[B]])(f: (A, B) => Z): Eval[Bin[Z]] = fa match {
    case Bin.Leaf => eleaf
    case Bin.Branch(a, la, ra) =>
      fb flatMap {
        case Bin.Leaf => eleaf
        case Bin.Branch(b, lb, rb) =>
          for {
            lz <- map2Eval(la, Eval.now(lb))(f)
            rz <- map2Eval(ra, Eval.now(rb))(f)
          } yield Bin.Branch(f(a, b), lz, rz)
      }

  }
}
