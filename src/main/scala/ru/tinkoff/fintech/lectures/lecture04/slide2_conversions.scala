package ru.tinkoff.fintech.lectures.lecture04

import scala.annotation.tailrec
import scala.language.implicitConversions

object slide2_conversions extends App {
  case class Complex[N: Fractional](real: N, imag: N) {
    import Fractional.Implicits._

    def +~(x: Complex[N]) = Complex(real + x.real, imag + x.imag)
    def -~(x: Complex[N]) = Complex(real - x.real, imag - x.imag)
    def *~(x: Complex[N]) = Complex(real * x.real - imag * x.imag, real * x.imag + imag * x.real)
    def reald             = real.toDouble
    def imagd             = imag.toDouble

    override def toString = f"$reald%f$imagd%+fi"
  }

//  println(Complex(1.0, 3.0))
//  println(Complex(1.0, 3.0) -~ Complex(2.0, 4.0))
//  println(Complex(1.0, 3.0) *~ Complex(2.0, 4.0))

  implicit def numToComplex[N: Fractional](i: N): Complex[N] = Complex(i, Fractional[N].zero)

  implicit def pairToComplex[N: Fractional](p: (N, N)): Complex[N] = Complex(p._1, p._2)

  import Math.{pow, sqrt}
  def abs(c: Complex[Double]): Double = sqrt(pow(c.real, 2) + pow(c.imag, 2))

  println(abs(1.0))
  println(1.0.real)
  println(1.0.imag)
  println((1.0, 3.0) -~ (2.0 -> 4.0))
  println((1.0, 3.0) *~ ((2.0, 4.0)))
}

object slide2_syntax_extensions extends App {
  implicit class RichLong(private val self: Long) extends AnyVal {
    def gcd(other: Long): Long =
      if (self < 0) (-self).gcd(other)
      else if (other > 0) gcd(-other)
      else if (self < other) other.gcd(self)
      else gcdInt(other)

    @tailrec private[RichLong] def gcdInt(other: Long): Long = if (other == 0) self else other.gcdInt(self % other)
  }

  println(12345L gcd 321)
}
