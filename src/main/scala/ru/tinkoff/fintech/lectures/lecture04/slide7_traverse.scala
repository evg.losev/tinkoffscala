package ru.tinkoff.fintech.lectures.lecture04

import cats.{Applicative, Eval, Id, Traverse}
import cats.implicits._

object slide7_traverse extends App {
  implicit val binTraverse: Traverse[Bin] = new BinTraverse {}

//  val x = List(1, 2, 3, 4).traverse(i => (s: String) => s"$s : $i")
//  println(x("foo"))
//  println(x("hello"))

  {
    type A    = Int
    type F[a] = List[a]
    type B    = String
    type G[b] = String => b

    implicitly[F[A] =:= List[Int]]
    implicitly[(A => G[B]) =:= (Int => String => String)]
    implicitly[G[F[B]] =:= (String => List[String])]
  }

//  val y = List(1, 2, 3, 4).traverse(i => (1, i.toString))

  {
    {
      type A    = Int
      type F[a] = List[a]
      type B    = String
      type G[b] = (Int, b)

      implicitly[F[A] =:= List[Int]]
      implicitly[(A => G[B]) =:= (Int => (Int, String))]
      implicitly[G[F[B]] =:= (Int, List[String])]
    }
  }

//  print(y)
//
// println(List(1, 2, 3, 4).traverse(i => (List(s"seen $i"), i / 2)))

  List(List(1, 2), List("a", "b", "c"), List(true, false)).sequence.foreach(println)

  println(Bin.example.zipWithIndex)
}

trait BinTraverse extends BinFunctor with BinFoldable with Traverse[Bin] {
  def traverse[G[_], A, B](fa: Bin[A])(f: A => G[B])(implicit G: Applicative[G]): G[Bin[B]] = fa match {
    case Bin.Leaf => G.pure(Bin.Leaf)
    case Bin.Branch(elem, left, right) =>
      (f(elem), traverse(left)(f), traverse(right)(f)).mapN(Bin.Branch(_, _, _))
  }

  def anotherTraverse[G[_], A, B](fa: Bin[A])(f: A => G[B])(implicit G: Applicative[G]): G[Bin[B]] = {
    def construct(elem: B)(left: Bin[B])(right: Bin[B]): Bin[B] = Bin.Branch(elem, left, right)

    lazy val go: Bin[A] => G[Bin[B]] = {
      case Bin.Leaf                      => Bin.empty[B].pure[G]
      case Bin.Branch(elem, left, right) => f(elem) map construct ap go(left) ap go(right)
    }
    go(fa)
  }

  def lawId[A](fa: Bin[A]) = traverse[Id, A, A](fa)(a => a: Id[A]) == fa
}
