package ru.tinkoff.fintech.lectures.lecture04

import cats.{Eval, Functor}
import cats.implicits._
import ru.tinkoff.fintech.lectures.Bin

object slide5_functor extends App {
  println(Bin.example.map(_.length))
  println(Bin.example.map(_ => 1))
}

trait BinFunctor extends Functor[Bin] {
  protected val eleaf = Eval.now(Bin.Leaf)

  def map[A, B](fa: Bin[A])(f: A => B): Bin[B] = {
    def go(x: Bin[A]): Eval[Bin[B]] = x match {
      case Bin.Leaf => eleaf
      case Bin.Branch(elem, left, right) =>
        for {
          l <- go(left)
          r <- go(right)
        } yield Bin.Branch(f(elem), l, r)
    }
    go(fa).value
  }
}
