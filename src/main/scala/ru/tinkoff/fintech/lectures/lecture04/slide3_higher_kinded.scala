package ru.tinkoff.fintech.lectures.lecture04

import cats.Foldable
import cats.data.Chain
import cats.kernel.Monoid
import cats.implicits._
import io.circe.{Encoder, Json}
import io.circe.syntax._

object slide3_higher_kinded extends App {
  // тайпклассы не обязательно объявлять для конкретного типа
  // можно определить для тайпконструктора, т.е. generic типа
  // чьи параметры можно варьировать в определении

  trait Aggregatable[Coll[_]] {
    def aggregateWith[A, B: Monoid](elements: Coll[A])(f: A => B): B
  }

  object Aggregatable {
    implicit val aggregatableBin: Aggregatable[Bin] = new Aggregatable[Bin] {
      def aggregateWith[A, B: Monoid](elements: Bin[A])(f: A => B): B = elements match {
        case Bin.Branch(elem, left, right) => f(elem) |+| aggregateWith(left)(f) |+| aggregateWith(right)(f)
        case Bin.Leaf                      => Monoid.empty
      }
    }

    implicit val aggregatableList: Aggregatable[List] = new Aggregatable[List] {
      def aggregateWith[A, B: Monoid](elements: List[A])(f: A => B): B =
        elements.foldLeft(Monoid.empty)((b, a) => b |+| f(a))
    }
  }

  def aggregateWith[Coll[_], A, B: Monoid](elements: Coll[A])(f: A => B)(implicit agg: Aggregatable[Coll]) =
    agg.aggregateWith(elements)(f)

  def jsonArr[Coll[_]: Aggregatable, A: Encoder](coll: Coll[A]): Json =
    Json.fromValues(aggregateWith(coll)(e => Chain.one(e.asJson)).toList)

//  println(jsonArr(Bin.example).spaces2)



  def foo[T[_[_]]](xs: T[List]): Unit = {}
  foo[Foldable](implicitly)

  // F[_]    => type => type
  // F[_, _] => (type, type) => type
  // F[_[_]] => (type => type) => type



}
