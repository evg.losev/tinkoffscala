package ru.tinkoff.fintech.lectures.lecture04

import cats.{Eval, Foldable}
import cats.implicits._
import cats.kernel.Order

object slide4_foldable extends App {
  implicit val bindFoldable: Foldable[Bin] = new BinFoldable {}

  println(Bin.example.toList)
  println(Bin.example.maximumOption)
  println((Bin.Leaf: Bin[String]).maximumOption)
  println(Bin.example.forall(_.forall(_.isDigit)))

  def onlyDigits[F[_]: Foldable](fa: F[String]): List[String] =
    fa.takeWhile_(_.forall(_.isDigit))

  println(onlyDigits("2" #:: "3" #:: "a" #:: LazyList.continually("1")))


}
trait BinFoldable extends Foldable[Bin] {
  def foldLeft[A, B](fa: Bin[A], b: B)(f: (B, A) => B): B = {
    def iter(stack: List[Bin[A]], b: B): B = stack match {
      case Bin.Branch(elem, l, r) :: rest => iter(l :: r :: rest, f(b, elem))
      case Bin.Leaf :: rest               => iter(rest, b)
      case Nil                            => b
    }
    iter(List(fa), b)
  }

  def foldRight[A, B](fa: Bin[A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = fa match {
    case Bin.Leaf                      => lb
    case Bin.Branch(elem, left, right) => Eval.defer(f(elem, foldRight(left, foldRight(right, lb)(f))(f)))
  }

}
