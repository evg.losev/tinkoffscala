package ru.tinkoff.fintech.lectures

import cats.{Apply, Eq, Functor}
import ru.tinkoff.fintech.lectures.lecture04.{BinApply, BinFunctor}

sealed trait Bin[+A]
object Bin {
  final case class Branch[A](elem: A, left: Bin[A], right: Bin[A])
    extends Bin[A]
  case object Leaf extends Bin[Nothing]

  def one[A](x: A): Bin[A] = Branch(x, Leaf, Leaf)
  def empty[A]: Bin[A] = Leaf

  val example: Bin[String] = Branch(
    "1",
    one("23"),
    Branch(
      "4",
      one(""),
      Branch("5", one("6"), Leaf)
    )
  )

  implicit def binEq[A: Eq]: Eq[Bin[A]] = Eq.fromUniversalEquals[Bin[A]]
//  implicit val binFunctor: Functor[Bin] = new BinFunctor {}
  implicit val binFunctor: Functor[Bin] = new BinFunctor {}
  implicit val binApply: Apply[Bin] = new BinApply {}


}
