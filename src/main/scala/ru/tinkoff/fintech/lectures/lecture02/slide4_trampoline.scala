package ru.tinkoff.fintech.lectures.lecture02

import scala.annotation.tailrec

sealed trait Tramp[+A] {
  def flatMap[B](f: A => Tramp[B]): Tramp[B] = Tramp.FlatMap(this, f)
  def map[B](f: A => B): Tramp[B]           = flatMap(a => Tramp.Pure(f(a)))
  def value: A                             = Tramp.run(this)
}
object Tramp {
  final case class Pure[+A](a: A)                               extends Tramp[A]
  final case class FlatMap[A, +B](ca: Tramp[A], f: A => Tramp[B]) extends Tramp[B]

  def unit: Tramp[Unit]                 = Pure(())
  def always[A](a: => A): Tramp[A]      = FlatMap(unit, (_: Unit) => Pure(a))
  def defer[A](a: => Tramp[A]): Tramp[A] = FlatMap(unit, (_: Unit) => a)

  @tailrec
  def run[A](c: Tramp[A]): A = c match {
    case Pure(a)                    => a
    case FlatMap(Pure(x), f)        => run(f(x)) // x: X, f: X => Calc[A]
    case FlatMap(FlatMap(cy, g), f) => run(cy.flatMap(b => g(b).flatMap(f))) // cy:Calc[Y], g: Y => Calc[X], f: X => Calc[A]
  }
}

object slide4_trampoline extends App {

  val bad = 1 to 10000 map (i => i -> true -> (i + 1)) toMap

  def genBinCalc[A](struct: Map[(A, Boolean), A])(root: A): Tramp[Bin[A]] = {
    def branch(tag: Boolean) =
      Tramp.defer(struct.get(root -> tag).map(genBinCalc(struct)).getOrElse(Tramp.Pure(Bin.Leaf)))
    for {
      left  <- branch(false)
      right <- branch(true)
    } yield Bin.Branch(root, left, right)
  }

  println(genBinCalc(bad)(1).value)
}
