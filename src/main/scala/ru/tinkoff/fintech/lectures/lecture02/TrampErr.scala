package ru.tinkoff.fintech.lectures.lecture02

sealed trait TrampErr[+E, +A] {
  def flatMap[E1 >: E, B](f: A => TrampErr[E1, B]): TrampErr[E1, B]    = TrampErr.FlatMap(this, f)
  def map[B](f: A => B): TrampErr[E, B]                            = flatMap(a => TrampErr.Pure(f(a)))
  def handleWith[A1 >: A, X](h: E => TrampErr[X, A1]): TrampErr[X, A1] = TrampErr.Catch(this, h)
  def mapError[X](h: E => X): TrampErr[X, A]                       = handleWith(e => TrampErr.Raise(h(e)))

  def result: Either[E, A] = TrampErr.run(this)
}
object TrampErr {
  final case class Pure[+A](a: A)                                         extends TrampErr[Nothing, A]
  final case class FlatMap[+E, A, +B](ca: TrampErr[E, A], f: A => TrampErr[E, B]) extends TrampErr[E, B]

  final case class Raise[+E](e: E)                                      extends TrampErr[E, Nothing]
  final case class Catch[E, +X, +A](ca: TrampErr[E, A], h: E => TrampErr[X, A]) extends TrampErr[X, A]

  def run[E, A](calc: TrampErr[E, A]): Either[E, A] = {
    def go[X, B](c: TrampErr[X, B]): Tramp[Either[X, B]] = c match {
      case Pure(a)  => Tramp.Pure(Right(a))
      case Raise(e) => Tramp.Pure(Left(e))
      case FlatMap(ca, f) =>
        Tramp.defer(go(ca)).flatMap {
          case Right(a) => go(f(a))
          case Left(e)  => Tramp.Pure(Left(e))
        }
      case Catch(ca, h) =>
        Tramp.defer(go(ca)).flatMap {
          case Right(a) => Tramp.Pure(Right(a))
          case Left(e)  => go(h(e))
        }
    }

    go(calc).value
  }
}
