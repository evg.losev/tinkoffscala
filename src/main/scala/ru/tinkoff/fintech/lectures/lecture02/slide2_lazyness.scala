package ru.tinkoff.fintech.lectures.lecture02

object slide2_lazyness extends App {
  def repeat[A](x: => A): LazyList[A] = {
    lazy val result: LazyList[A] = x #:: result
    result
  }

//  println(repeat("a").take(100).force)








  def cycle[A](xs: LazyList[A]): LazyList[A] = {
    lazy val result: LazyList[A] = xs #::: result
    result
  }
   //O(1), O(N)

//  val lz = cycle(LazyList("a", "b", "c")).take(100000).map(_(0).toInt)
//  println(lz.sum)



  def collatz(x: => Long): LazyList[Long] = x #:: (if (x % 2 == 0) collatz(x / 2) else collatz(3 * x + 1))

//  println(collatz(27).takeWhile(_ != 1).force)

  val collatzL: (=> ((Long) => LazyList[Long])) => (Long) => LazyList[Long] =
    recur => x => x #:: (if (x % 2 == 0) recur(x / 2) else recur(3 * x + 1))

  def fixn[A](f: (=> A) => A): A = f(fixn(f))

  println(fixn(collatzL)(27).takeWhile(_ != 1).sum)

  def foo(x: Int, b: Boolean) : String = {
    println("applying foo")
    if(!b) "unknown" else (x + x).toString
  }

  def fooBN(x: => Int, b: Boolean) : String = {
    println("applying foo by name")
    if(!b) "unknown" else (x + x).toString
  }

  def fooByThunk(x: () => Int, b: Boolean) : String = {
    println("applying foo by name")
    if(!b) "unknown" else (x() + x()).toString
  }

  def x = {
    println("calculating x")
    5
  }

//  println(foo(x, true))
//  println(fooBN(x, true))
//  println(foo(x, false))
//  println(fooBN(x, false))




  // Y - combinator
  def fix[A](f: (=> A) => A): A = {
    lazy val r: A = f(r)
    r
  }

//  println(fix(collatzL)(27).takeWhile(_ != 1).sum)

  println(
    fix[(Long, Long) => Long](
      rec => (x, acc) => if (x == 1) acc else if (x % 2 == 0) rec(x / 2, acc + x) else rec(3 * x + 1, acc + x)
    )(27, 0)
  )


    println(
      fix[(Long, Long, Long) => Long](
        rec => (a, b, acc) => if (a >= b) acc else rec(a + 1, b, acc + a)
      )(0, 100, 0)
    )

}
