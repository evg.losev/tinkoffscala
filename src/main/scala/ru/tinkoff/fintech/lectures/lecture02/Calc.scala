package ru.tinkoff.fintech.lectures.lecture02

final case class Calc[+E, +A](value: Tramp[Either[E, A]]) extends AnyVal {
  def flatMap[E1 >: E, B](f: A => Calc[E1, B]): Calc[E1, B] =
    Calc(value.flatMap {
      case Left(e)  => Tramp.Pure(Left(e))
      case Right(a) => f(a).value
    })
  def map[B](f: A => B): Calc[E, B] = flatMap(a => Calc.pure(f(a)))

  def handleWith[A1 >: A, X](h: E => Calc[X, A1]): Calc[X, A1] =
    Calc(value.flatMap {
      case Left(e)  => h(e).value
      case Right(a) => Tramp.Pure(Right(a))
    })
  def mapError[X](h: E => X): Calc[X, A] = handleWith(e => Calc.raise(h(e)))

  def result: Either[E, A] = value.value

  def handle[A1 >: A](h: E => A1): Calc[Nothing, A1] = handleWith(e => Calc.pure(h(e)))

  def *>[E1 >: E, B](other: => Calc[E1, B]): Calc[E1, B] = flatMap(_ => other)

  def attempt: Calc[Nothing, Either[E, A]] = map(Right(_)).handle(Left(_))

  def flatTap[E1 >: E, B](f: A => Calc[E1, B]): Calc[E1, A] = flatMap(x => f(x).map(_ => x))
}
object Calc {
  def pure[A](a: A): Calc[Nothing, A]  = Calc(Tramp.Pure(Right(a)))
  def raise[E](e: E): Calc[E, Nothing] = Calc(Tramp.Pure(Left(e)))

  val unit: Calc[Nothing, Unit] = Calc.pure(())

  def fromEither[E, A](e: Either[E, A]): Calc[E, A] = Calc(Tramp.Pure(e))

  def fromOption[E, A](oa: Option[A], e: => E) = Calc(Tramp.Pure(oa.toRight(e)))
}
