package ru.tinkoff.fintech.lectures.lecture02

import cats.data.NonEmptyList
import cats.syntax.either._
import cats.syntax.parallel._
import cats.instances.parallel._

object slide5_errors extends App {
  sealed trait Error
  final case class ParseError(s: String) extends Error
  final case class NumIsEven(i: Int)     extends Error

  def readAll[E, A](input: List[String])(read: String => Calc[E, A])(validate: A => Calc[E, Any]): Calc[E, List[A]] =
    input match {
      case Nil => Calc.pure(Nil)
      case str :: rest =>
        for {
          a    <- read(str)
          _    <- validate(a)
          tail <- readAll(rest)(read)(validate)
        } yield a :: tail
    }

  def readInt(s: String): Calc[Error, Int]  = Calc.fromOption(s.toIntOption, ParseError(s))
  def checkIsOdd(i: Int): Calc[Error, Unit] = if (i % 2 == 0) Calc.raise(NumIsEven(i)) else Calc.pure(i)

  println(readAll[Error, Int](List("123123", "231", "1236"))(readInt)(checkIsOdd).result)

  def readAllAcc[E, A](
      input: List[String]
  )(read: String => Calc[E, A])(validate: A => Calc[E, Any]): Calc[NonEmptyList[E], List[A]] =
    input match {
      case Nil => Calc.pure(Nil)
      case str :: rest =>
        for {
          ea    <- read(str).flatTap(validate).attempt.map(_.toEitherNel)
          etail <- readAllAcc[E, A](rest)(read)(validate).attempt
          res   <- Calc.fromEither((ea, etail).parMapN(_ :: _))
        } yield res
    }

  println(readAllAcc[Error, Int](List("123123", "231b", "1236"))(readInt)(checkIsOdd).result)


}
