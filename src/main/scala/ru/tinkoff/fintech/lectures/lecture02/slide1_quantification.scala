package ru.tinkoff.fintech.lectures.lecture02

object slide1_quantification extends App {

  // как много элементов у следующих типов
  type X = Boolean
  def booleans: List[Boolean] = List(false, true)

  sealed trait Ternary
  object Ternary {
    case object Yes   extends Ternary
    case object No    extends Ternary
    case object Maybe extends Ternary
  }

  import Ternary._

  def ternaries: List[Ternary] = List(No, Maybe, Yes)

  // 2 * 2
  type X1 = (Boolean, Boolean)

  def x1s: List[X1] = for (a <- booleans; b <- booleans) yield (a, b)

  // 2 * 3
  type X2 = (Boolean, Ternary)

  def x2s: List[X2] = for (a <- booleans; b <- ternaries) yield (a, b)

  def product[A, B](as: List[A], bs: List[B]) = for (a <- as; b <- bs) yield (a, b)

  // 2 + 3
  type X3 = Either[Boolean, Ternary]

  def x3s: List[X3] = (for (a <- booleans) yield Left(a)) ++ (for (b <- ternaries) yield Right(b))

  //3 + 3
  type X4 = Either[Ternary, Ternary]

  def x4s: List[X4] = (for (a <- ternaries) yield Left(a)) ++ (for (b <- ternaries) yield Right(b))

  def sum[A, B](as: List[A], bs: List[B]) = (for (a <- as) yield Left(a)) ++ (for (b <- bs) yield Right(b))

  // 2 x 3 == 3 + 3
  Iso[(Boolean, Ternary), Either[Ternary, Ternary]] {
    case (false, t) => Left(t)
    case (true, t)  => Right(t)
  } {
    case Left(t)  => (false, t)
    case Right(t) => (true, t)
  }

  type X5 = Boolean => Boolean

  // 2 * 2 = 2 ^ 2
  def x5s: List[X5] = List(
    Map(true -> true, false  -> true),  // const true
    Map(true -> false, false -> false), // const false
    Map(true -> true, false  -> false), // id
    Map(true -> false, false -> true)   // not
  )

  def x5s_2: List[X5] = List(
    { case true => true; case false  => true },  // const true
    { case true => false; case false => false }, // const false
    { case true => true; case false  => false }, // id
    { case true => false; case false => true }   // not
  )
  //9 = 3 ^ 2 = 3 * 3
  type X6 = Boolean => Ternary
  type X61 = (Ternary, Ternary)

  def x6s: List[X6] =
    List(
      Map(true -> Yes, false   -> Yes),
      Map(true -> Yes, false   -> No),
      Map(true -> Yes, false   -> Maybe),
      Map(true -> No, false    -> Yes),
      Map(true -> No, false    -> No),
      Map(true -> No, false    -> Maybe),
      Map(true -> Maybe, false -> Yes),
      Map(true -> Maybe, false -> No),
      Map(true -> Maybe, false -> Maybe)
    )

  type X7 = Ternary => Boolean
  type X71 = (Boolean, Boolean, Boolean)

  def x7s: List[X7] = List(
    Map(Yes -> true, No  -> true, Maybe  -> true),
    Map(Yes -> true, No  -> true, Maybe  -> false),
    Map(Yes -> true, No  -> false, Maybe -> true),
    Map(Yes -> true, No  -> false, Maybe -> false),
    Map(Yes -> false, No -> true, Maybe  -> true),
    Map(Yes -> false, No -> true, Maybe  -> false),
    Map(Yes -> false, No -> false, Maybe -> true),
    Map(Yes -> false, No -> false, Maybe -> false)
  )

  import cats.syntax.traverse._
  import cats.instances.list._
  def exponents[A, B](as: List[A], bs: List[B]): List[A => B] =
    List
      .fill(as.size)(bs)
      .foldLeft(List(List[B]()))(
        (ls, bs) => for (l <- ls; b <- bs) yield b :: l
      ) // or just .sequence
      .map(bs => as.zip(bs).toMap)

  exponents(ternaries, booleans).foreach(println)

}
