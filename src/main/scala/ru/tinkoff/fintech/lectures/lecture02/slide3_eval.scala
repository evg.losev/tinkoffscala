package ru.tinkoff.fintech.lectures.lecture02

import cats.Eval
import cats.data.Chain
import cats.implicits._
import ru.tinkoff.fintech.lectures.lecture02.slide3_eval.LazyProdStr

sealed trait Bin[+A] extends LazyProdStr
object Bin {
  final case class Branch[A](elem: A, left: Bin[A], right: Bin[A]) extends Bin[A]
  case object Leaf                                                 extends Bin[Nothing]
}

object slide3_eval extends App {
  println(Eval.now(100))
  println(Eval.later(100))
  println(Eval.always(100))

  println(Eval.later(1 + 3).value)

//  Random.setSeed(1644527396)
//  val alpha = 20 to 300 map (_.toChar)
//  val str   = Iterator.fill(1000)(alpha(Random.nextInt(alpha.size))).mkString
//  println(str)

  def genBin[A](struct: Map[(A, Boolean), A])(root: A): Bin[A] = {
    def branch(tag: Boolean) = struct.get((root , tag)).map(genBin(struct)).getOrElse(Bin.Leaf)
    val left                 = branch(false)
    val right                = branch(true)
    Bin.Branch(root, left, right)
  }

//  println(
//    genBin(
//      Map(
//        "a" -> false -> "b",
//        "a" -> true  -> "c",
//        "b" -> false -> "d",
//        "c" -> true  -> "e"
//      )
//    )("a")
//  )

  val bad = 1 to 10000 map (i => i -> true -> (i + 1)) toMap

//  println(genBin(bad)(1))

  def genBinLz[A](struct: Map[(A, Boolean), A])(root: A): Eval[Bin[A]] = {
    def branch(tag: Boolean) =
      Eval.defer(struct.get(root -> tag).map(genBinLz(struct)).getOrElse(Eval.now(Bin.Leaf)))
    for {
      left  <- branch(false)
      right <- branch(true)
    } yield Bin.Branch(root, left, right)
  }
  genBinLz(bad)(1).value
  println(genBinLz(bad)(1).value)

  trait LazyProdStr extends Product {
//    def toStringLzs: Eval[String] =
//      Eval.defer(productIterator.toList.traverse {
//        case lz: LazyProdStr => lz.toStringLzs
//        case strict          => Eval.later(strict.toString)
//      }.map(_.mkString(productPrefix + "(", ",", ")")))
//
//    override def toString = toStringLzs.value.mkString

    import cats.data.Chain.{fromSeq => chain}
    def toStringLz: Eval[Chain[Char]] =
      Eval.defer(chain(productIterator.toList).traverse {
        case lz: LazyProdStr => lz.toStringLz
        case strict          => Eval.later(chain(strict.toString))
      }.map(_.foldSmash(chain(productPrefix + "("), chain(","), chain(")"))))

    override def toString = toStringLz.value.iterator.mkString
  }

}
