package ru.tinkoff.fintech.lectures.lecture02

trait Iso[A, B] {
  def to(a: A): B
  def from(b: B): A
}

object Iso {
  def apply[A, B](f: A => B)(g: B => A): Iso[A, B] = new Iso[A, B] {
    def to(a: A): B   = f(a)
    def from(b: B): A = g(b)
  }

  def forallA[A, B](a: A, iso: Iso[A, B]) = iso.from(iso.to(a)) == a
  def forallB[A, B](b: B, iso: Iso[A, B]) = iso.to(iso.from(b)) == b
}
