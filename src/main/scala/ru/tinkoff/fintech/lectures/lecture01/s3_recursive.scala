package ru.tinkoff.fintech.lectures.lecture01

object s3_recursive extends App {
  // recursive ADT
  sealed trait Organization
  final case class User(firstName: String, lastName: String)                      extends Organization
  final case class Department(name: String, head: User, subs: List[Organization]) extends Organization

  def naiveGetUsers(org: Organization): List[User] = org match {
    case user: User                => List(user)
    case Department(_, boss, subs) => boss :: subs.flatMap(naiveGetUsers)
  }

  // tail recusive version
  def genOrgRec(n: Int): Department = {
    def go(acc: Department, remain: Int): Department =
      if (remain == 0) acc
      else go(Department(s"org#$remain", User(s"Босс$remain", "Боссов"), List(acc)), remain - 1)

    go(Department("lastOrg", User("Босс", "Боссов"), Nil), n)
  }

  def genOrg(n: Int): Department =
    (n until 0).foldLeft(Department("lastOrg", User("Босс", "Боссов"), Nil)) { (acc, remain) =>
      Department(s"org#$remain", User(s"Босс$remain", "Боссов"), List(acc))
    }

//  println(genOrg(2))

//  println(naiveGetUsers(naiveGetUsersgenOrgRec(10000)))

  def getUsersTailRec(org: Organization): List[User] = {
    def go(orgs: List[Organization], acc: List[User]): List[User] = orgs match {
      case Nil                                   => acc
      case (user: User) :: rest                  => go(rest, user :: acc)
      case Department(_, boss, children) :: rest => go(children ::: rest, boss :: acc)
    }

    go(List(org), Nil)
  }

//  println(getUsersTailRec(genOrgRec(10000)))

  def getUsersLz(org: Organization): LazyList[User] = org match {
    case user: User                => LazyList(user)
    case Department(_, boss, subs) => boss #:: subs.to(LazyList).flatMap(getUsersLz)
  }

//  println(getUsersLz(genOrgRec(5000)).toList) // will fail on 10K

  import cats.arrow.FunctionK
  import cats.free.Free
  import cats.instances.list._
  def getUsersFree(org: Organization): Free[List, User] = org match {
    case user: User                => Free.pure(user)
    case Department(_, boss, subs) => Free.roll(List(Free.pure(boss), Free.liftF(subs).flatMap(getUsersFree)))
  }

//  println(getUsersFree(genOrgRec(100000)).foldMap(FunctionK.id)) // wont fail
}
