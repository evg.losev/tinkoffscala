//package ru.tinkoff.fintech.lectures.lecture01.trying
//
//class TryL1V1
//
//import cats.effect.Sync
//import cats.effect.concurrent.Ref
//import cats.mtl.{ ApplicativeAsk, MonadState }
//import cats.{ FlatMap, Monad }
//import ru.tinkoff.fintech.lectures.lecture01.trying.effects._
//import cats.implicits._
//
//trait Program[F[_]] {
//  def getUsers: F[List[User]]
//  def changeUsers: F[Unit]
//}
//
//final case class User(name: String)
//final case class Program3InvMutable[F[_]](users: List[User], pingService: PingService[F])
//
//trait PingService[F[_]] {
//  def isActive(user: User): F[Boolean]
//}
//
//object PingService {
//  def apply[F[_]]: F[PingService[F]] = ???
//}
//
////подход 1
//class Program3Impl[F[_]: Monad](ref: Ref[F, Program3InvMutable[F]]) extends Program[F] {
//  override def getUsers: F[List[User]] = ref.get.map(_.users)
//
//  override def changeUsers: F[Unit] =
//    for {
//      env         <- ref.get
//      usersStatus <- env.users.traverse(user => env.pingService.isActive(user).map(result => user -> result))
//      activeUser  <- usersStatus.filter(_._2 == true).pure[F]
//      _           <- ref.update(_.copy(users = activeUser.map(_._1)))
//    } yield ()
//}
//object xcvb {
//  def createProgram3[F[_] : Sync]: F[Program3Impl[F]] =
//    for {
//      service <- PingService.apply[F]
//      ref <- Ref.of[F, Program3InvMutable[F]](Program3InvMutable[F](List.empty, service))
//    } yield new Program3Impl[F](ref)
//
//  //подход 2
//  class Program4Impl[F[_] : Monad](implicit MS: MonadState[F, Program3InvMutable[F]]) extends Program[F] {
//    override def getUsers: F[List[User]] = MS.get.map(_.users)
//
//    override def changeUsers: F[Unit] =
//      for {
//        env <- MS.get
//        usersStatus <- env.users.traverse(user => env.pingService.isActive(user).map(result => user -> result))
//        activeUser <- usersStatus.filter(_._2 == true).pure[F]
//        _ <- MS.modify(_.copy(users = activeUser.map(_._1)))
//      } yield ()
//  }
//
//  def createProgram4[F[_] : Sync]: F[Program4Impl[F]] =
//    for {
//      service <- PingService.apply[F]
//      ref <- Ref.of[F, Program3InvMutable[F]](Program3InvMutable(List.empty, service))
//      ms <- ref.runState { implicit state =>
//        new Program4Impl[F].pure[F]
//      }
//    } yield ms
//}