package ru.tinkoff.fintech.lectures.lecture01

object s0_classes extends App{
  class User(val name: String, val age: Int) extends Serializable {
    def copy(name: String = this.name, age: Int = this.age): User = User(name, age)
  }

  object User {
    def apply(name: String, age: Int): User = new User(name, age)
  }


  val user1 = User("Вася", 23)

  val user2 = user1.copy(age = 34)

  val user3 = user2.copy(name = "Женя")

  val user4 = user1.copy(name = "Саша")

  for(user <- List(user1, user2, user3, user4))
    println(user)
}
