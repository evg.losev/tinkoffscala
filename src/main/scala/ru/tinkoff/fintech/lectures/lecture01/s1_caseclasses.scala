package ru.tinkoff.fintech.lectures.lecture01

import ru.tinkoff.fintech.lectures.lecture01.s1_caseclasses.User

object s1_caseclasses extends App {
  case class User(name: String, age: Int, company: String)

  val user1 = User("Вася", 23, "Tinkoff")

  val user2 = user1.copy(age = 34)

  val user3 = user2.copy(name = "Женя", company = "Dialog")

  val user4 = user1.copy(name = "Саша")

  val user5 = user4.copy(name = "Вася").copy(age = 23)

  def checks1() = {
    println(user1, user5)

    println(user1.hashCode, user5.hashCode)
    println(user1 == user5)

    println(classOf[User].getInterfaces.toList)
    println(classOf[User].getSuperclass)
  }

  def byAge(users: List[User]): Map[Int, List[String]] =
    users.groupMap(_.age) {
      case User(name, _, "Tinkoff") => s"$name"
      case User(name, _, cmp)       => s"$name from $cmp"
    }


  // varargs
  def byAge(users: User*): Map[Int, List[String]] = byAge(users.toList)

  def checks2() =
    println(byAge(user1, user2, user3, user4, user5))

  checks2()
}
