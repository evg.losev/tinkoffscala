package ru.tinkoff.fintech.lectures.lecture01

object s4_polymorphic {
  sealed trait Organization[+A] {
    def elem: A

    def children: List[Organization[A]]

    private def elementIterator: Iterator[A] = this match {
      case Leaf(a)               => Iterator(a)
      case Branch(_, elem, subs) => Iterator(elem) ++ subs.iterator.flatMap(_.elementIterator)
    }

    def elements: List[A] = elementIterator.toList

    def map[B](f: A => B): Organization[B] = this match {
      case Leaf(a)                  => Leaf(f(a))
      case Branch(name, elem, subs) => Branch(name, f(elem), subs.map(_.map(f)))
    }

//    def addChildren(name: String, children: List[Organization[A]]): Organization[A] = this match {
//      case Leaf(a)              => Branch(name, a, children)
//      case Branch(name, a, own) => Branch(name, a, own ++ children)
//    }

    def addChildren[A1 >: A](name: String, children: List[Organization[A1]]): Organization[A1] = this match {
      case Leaf(a)              => Branch(name, a, children)
      case Branch(name, a, own) => Branch(name, a, own ++ children)
    }
  }

  final case class Leaf[+A](elem: A) extends Organization[A] {
    override def children = Nil
  }
  final case class Branch[+A](name: String, elem: A, children: List[Organization[A]]) extends Organization[A]

}
