package ru.tinkoff.fintech.homeworks.homework1

import ru.tinkoff.fintech.homeworks.Homeworks._

object task4_lazy_list extends App {
  lazy val primes: Stream[Long] = 2 #:: genEvenStream(3).filter(isPrime)

  /** проверка на простоту числа - нужно сделать с помощью `primes` */
  def isPrime(n: Long): Boolean = n > 1 && (2L to Math.sqrt(n).toLong).forall(x => n % x != 0)
  def genEvenStream(n: Long): Stream[Long] = if (isPrime(n)) n #:: genEvenStream(n + 1)
  else genEvenStream(n + 1)
//  println("простые числа - имеющие ровно два делителя, себя и 1. Числа 2,3,5,7,11...")
//  println(primes.take(100).force)
//  println("")

}
