package ru.tinkoff.fintech.homeworks.homework1

import ru.tinkoff.fintech.homeworks.Homeworks._

import scala.collection.mutable.ListBuffer

object task3_maxima {

  /**
    *  В данном задании предлагаем Вам из исходного списка целых чисел
    *  сконструировать список его нестрогих локальных максимумов.
    *
    *  Максимум является нестрогим, если он не меньше своих соседей по списку.
    *
    *  Если у элемента меньше двух соседей, то он НЕ считается локальным максимумом.
    * */
  /**
    * @param ints список чисел, среди которых необходимо найти нестрогие локальные максимумы
    * @return список найденных нестрогих локальных максимумов
    */

  def maxima(ints: List[Int]): List[Int] = {
    if (ints.size < 3) return Nil
    val x = ints.sliding(3).foldLeft(ListBuffer[Int]()){
    case (seed, Seq(a, b, c)) => if(b >= a && b >= c) seed += b else seed
  }
    x.toList
  }
}
