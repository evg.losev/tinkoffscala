package ru.tinkoff.fintech.seminars.seminar1.seminar3

import java.time.LocalDate

import cats.data.{Validated, ValidatedNec, ValidatedNel}
import cats.data.Validated.{Invalid, Valid}
import cats.implicits._

case class Passport(number: String, validUntil: LocalDate)
case class Person(age: Int, country: String, passport: Passport)

object Person {
  val ivan = Person(17, "Belarus", Passport("24", LocalDate.of(2020, 6, 1)))
  val victor = Person(35, "Krakozhia", Passport("42", LocalDate.of(2030, 1, 1)))
}

sealed trait Visa

object Visa {
  case class Approved(id: String) extends Visa
  case class Refused(reason: String) extends Visa
}

object eitherValidation {
  import Visa._
  def validateAge(age: Int): Either[Refused, Int] =
    Either.cond(age >= 18, age, Refused("Too young"))

  def validateCountry(country: String): Either[Refused, String] =
    Either.cond(country == "Krakozhia", country, Refused("Not from Krakozhia"))

  def validatePassport(passport: Passport): Either[Refused, Passport] =
    Either.cond(passport.validUntil.isAfter(LocalDate.of(2021, 1, 1)),
                passport,
                Refused("Old passport"))

  def validatePerson(person: Person): Either[Refused, Approved] =
    for {
      _ <- validateAge(person.age)
      _ <- validateCountry(person.country)
      passport <- validatePassport(person.passport)
    } yield Approved(passport.number)
}

object slide1_either_validation extends App {
  import eitherValidation._
  import Person._

  val visaForIvan = validatePerson(ivan)
  val visaForVictor = validatePerson(victor)

  println(visaForIvan)
  println(visaForVictor)
}

object validatedValidation {
  import Visa._
  def validateAge(age: Int): ValidatedNel[Refused, Int] =
    if (age >= 18)
      age.validNel
    else
      Refused("Too young").invalidNel

  def validateCountry(country: String): ValidatedNel[Refused, String] =
    if (country == "Krakozhia")
      country.validNel
    else Refused("Not from Krakozhia").invalidNel

  def validatePassport(passport: Passport): ValidatedNel[Refused, Passport] =
    if (passport.validUntil.isAfter(LocalDate.of(2021, 1, 1)))
      passport.validNel
    else
      Refused("Old passport").invalidNel

  def validatePerson(person: Person): ValidatedNel[Refused, Approved] =
    (validateAge(person.age),
     validateCountry(person.country),
     validatePassport(person.passport)).mapN {
      case (age, country, passport) =>
        Approved(passport.number)
    }
}

object slide1_vaildated extends App {
  import validatedValidation._
  import Person._

  val visaForIvan = validatePerson(ivan)
  val visaForVictor = validatePerson(victor)

  println(visaForIvan)
  println(visaForVictor)
}

object slide1_validated_aux extends App {
  import validatedValidation._
  import Visa._
  import Person.ivan._

  val visaForIvan = (validateAge(age), validateCountry(country).andThen(_ => validatePassport(passport))).mapN{
    case (age, passport) => Approved(passport.number)
  }

  val ageValid = validateAge(age)
  val countryValid = validateCountry(country)
  val passportValid = validatePassport(passport)

  val result =
    (ageValid, countryValid, passportValid).mapN{
      case (_, _,  passport) =>
        Approved(passport.number)
    }

  println(result)
}