package ru.tinkoff.fintech.seminars.seminar1.seminar2

import scala.util.Try

object slide1_syntax extends App {

  object eitherSyntax {

    // implicit class - обозначает, что для типа, который стоит в списке параметров.
    // Здесь для Either[E1, A] добавляются методы leftMap, leftFlatMap.
    // Они появятся, только если импортировать класс EitherOps (import eitherSyntax._)
    // extends AnyVal - специальная оптимизация - https://docs.scala-lang.org/overviews/core/value-classes.html
    implicit class EitherOps[E1, A](val either: Either[E1, A]) extends AnyVal {
      def leftMap[E2](f: E1 => E2): Either[E2, A] =
        either match {
          case Left(value)  => Left(f(value))
          case Right(value) => Right(value)
        }

      def leftFlatMap[E2](f: E1 => Either[E2, A]): Either[E2, A] =
        either match {
          case Left(value)  => f(value)
          case Right(value) => Right(value)
        }
    }

  }

  val right: Either[String, Int] = Right(123)
  val left1: Either[String, Int] = Left("Ooops! Error")
  val left2: Either[String, Int] = Left("1337")

  import eitherSyntax._
//  Синтаксис из eitherSyntax и многое другое реализованы в
//  import cats.syntax.either._

  println(right.leftMap(_.reverse))
  println(left1.leftMap(_.reverse))
  println(left2.leftMap(_.reverse))
  println()
  println(right.leftFlatMap(l => Try(l.toInt).toEither))
  println(left1.leftFlatMap(l => Try(l.toInt).toEither))
  println(left2.leftFlatMap(l => Try(l.toInt).toEither))
}
