package ru.tinkoff.fintech.seminars.seminar1.tryOptionEither

object s6_for_comprehension extends App {
  // Если закомментировать .map или .flatMap, то for-comprehension ниже не скомпилируктся
  sealed trait MyOption[+A] {
    def map[B](f: A => B): MyOption[B]
    def flatMap[B](f: A => MyOption[B]): MyOption[B]
  }

  case class MySome[+A](value: A) extends MyOption[A] {
    def map[B](f: A => B) =
      MySome(f(value))

    def flatMap[B](f: A => MyOption[B]): MyOption[B] =
      f(value)
  }

  case object MyNone extends MyOption[Nothing] {
    def map[B](f: Nothing => B): MyOption[B] = MyNone

    def flatMap[B](f: Nothing => MyOption[B]): MyOption[B] = MyNone
  }

  val f: Int => MyOption[String] =
    int => MySome(int.toString)

  val g: String => MyOption[String] =
    str => MySome(str.reverse)

  for {
    a <- f(1234)
    _ = println(a)
    b <- g(a)
    _ = println(b)
  } yield b
}

object s6_cats extends App {
  import cats.syntax.option._

  val someInt: Option[Int] = 1.some
  println(someInt)


  import cats.syntax.either._

  val leftString = "someError".asLeft
  val rightInt = 1.asRight
  println(leftString)
  println(rightInt)

  // Как .map и .flatMap, но для Left
  println(leftString.leftMap(_.reverse))
  println(leftString.leftFlatMap(_.toIntOption.toRight("It's not an int")))


  // В пакетах с синтаксисом также много методов для преобразования Option и Either в типы данных из cats
  import cats.data.EitherNel
  // type EitherNel[+E, +A] = Either[NonEmptyList[E], A]
  val result1: EitherNel[String, Int] = "error1".asLeft[Int].toEitherNel
  val result2: EitherNel[String, Int] = 1.asRight[String].toEitherNel
  val result3: EitherNel[String, Int] = "error2".asLeft[Int].toEitherNel

  import cats.syntax.parallel._
  import cats.instances.parallel._

  val eitherNel: EitherNel[String, String] = (result1, result2, result3).parMapN(_ + "," + _ + "," + _ )
  println(eitherNel)
}