package ru.tinkoff.fintech.seminars.seminar1.tryOptionEither

object s6_task1 extends App {
  case class Item(name: String)
  val database = Map(
    "1" -> Item("cabbage"),
    "2" -> Item("spinach")
  )
  def getFromDataBase(id: String): Either[String, Item] =
    database.get(id).toRight(s"Item '$id' not found!")
  def getAllFromDataBase(ids: List[String]): Either[String, List[Item]] = ???

  println(getAllFromDataBase(List("1", "2", "3", "4"))) // should fail for "3"
  println(getAllFromDataBase(List("2", "1"))) // should work fine
}
