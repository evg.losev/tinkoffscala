package ru.tinkoff.fintech.seminars.seminar1.tryOptionEither

import cats.data.{EitherNel, NonEmptyList}
import cats.syntax.parallel._
import cats.syntax.either._
import cats.syntax.option._
import cats.syntax.flatMap._
import cats.instances.parallel._
import cats.instances.either._

object s3_smartCtr1 extends App {

  // верифицированный кейс класс
  // нельзя сконструировать значение типа, не соответствующее условиям

  sealed abstract case class Age(age: Int)

  object Age {
    def apply(age: Int): Either[String, Age] =
      if (age < 0) Left("age could not be negative")
      else if (age > 200) Left("age could not be more than 200 years")
      else Right(new Age(age) {})
  }

  println(Age(-3))
  println(Age(0))
  println(Age(11))

  sealed abstract case class Name(name: String)

  object Name {
    def apply(name: String): Either[String, Name] =
      if (name.isEmpty) Left("name should not be empty")
      else if (name.length > 200) Left("name is too long")
      else Right(new Name(name) {})
  }

  println(Name(""))
  println(Name("na" * 100 + "Batman"))
  println(Name("Alexander"))
}

object s3_smartCtr2 extends App {
  // верифицированный комплексный тип

  import s3_smartCtr1._

  sealed abstract case class User(name: Name, age: Age)

  object User {
    def apply(name: String, age: Int): Either[String, User] =
      for {
        name <- Name(name)
        age <- Age(age).filterOrElse(_.age <= 16, "user is too young")
      } yield new User(name, age) {}

    def accum(name: EitherNel[String, Name], age: EitherNel[String, Age]): EitherNel[String, User] =
      (name, age.flatTap(age => if (age.age <= 16) "user is too young".leftNel else ().asRight))
        .parMapN(new User(_, _) {})

    def of(name: String, age: Int): EitherNel[String, User] = accum(Name(name).toEitherNel, Age(age).toEitherNel)
  }

  println(User("Oleg", 34))

  println(User("", 11))

  println(User("Oleg", -4))

  // ошибки можно аккумулировать с помощью специальных типов

//  println(User.of("Oleg", 34))

//  println(User.of("", 11))

//  println(User.of("Oleg", -4))
}

object s3_smartCtr3 extends App {
  import s3_smartCtr1._
  import s3_smartCtr2._
  // builder - нечастно применяющийся паттерн конструкции, где данные появляются частями

  case class BuildUser(name: Option[String] = None, age: Option[Int] = None) {
    def toUser: Either[String, User] =
      for {
        nameF <- name.toRight("name is not defined")
        ageF  <- age.toRight("age is not defined")
        user  <- User(nameF, ageF)
      } yield user
  }
  val builder1 = BuildUser(age = 11.some)
  val builder2 = builder1.copy(name = "Oleg".some)
  val builder3 = builder2.copy(age = 34.some)

  println(builder1.toUser)
  println(builder2.toUser)
  println(builder3.toUser)

}
