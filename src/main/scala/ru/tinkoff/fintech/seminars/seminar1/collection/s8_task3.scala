package ru.tinkoff.fintech.seminars.seminar1.collection

object s8_task3 extends App {
  val map1 = Map("vasily" -> 100, "oleg"     -> 50)
  val map2 = Map("oleg"   -> 40, "alexander" -> 93)

  def unionWith(map1: Map[String, Int], map2: Map[String, Int])(f: (Int, Int) => Int): Map[String, Int] = ???

  println(unionWith(map1, map2)(_ + _))
}
