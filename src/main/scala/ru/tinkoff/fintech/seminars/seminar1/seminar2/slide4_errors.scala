package ru.tinkoff.fintech.seminars.seminar1.seminar2

import java.util.NoSuchElementException

import cats.{Eval, MonadError}

object slide4_errors extends App {
  type EvalErr[A] = Eval[Either[Throwable, A]]

  implicit val evalMonadError: MonadError[EvalErr, Throwable] =
    new MonadError[EvalErr, Throwable] {

      def raiseError[A](e: Throwable): EvalErr[A] =
        Eval.now(Left(e))

      def handleErrorWith[A](fa: EvalErr[A])(f: Throwable => EvalErr[A]): EvalErr[A] =
        fa.flatMap {
          case Left(value)  => f(value)
          case r @ Right(_) => Eval.later(r)
        }

      def pure[A](x: A): EvalErr[A] =
        Eval.now(Right(x))

      def flatMap[A, B](fa: EvalErr[A])(f: A => EvalErr[B]): EvalErr[B] =
        fa.flatMap {
          case Left(value)  => Eval.later(Left(value))
          case Right(value) => f(value)
        }

      // Стекобезопасен, т.к. flatMap у Eval стекобезопасен
      def tailRecM[A, B](a: A)(f: A => EvalErr[Either[A, B]]): EvalErr[B] =
        f(a).flatMap {
          case Left(value)         => Eval.later(Left(value))
          case Right(Right(value)) => Eval.later(Right(value))
          case Right(Left(value))  => tailRecM(value)(f)
        }
    }

  // Следующие 4 строчки можно заменить на import tofu.syntax.monadic._
  import cats.syntax.applicative._ // Метод .pure
  import cats.syntax.functor._     // Метод .map
  import cats.syntax.flatMap._     // Метод .flatMap
  import cats.syntax.monad._       // Метод .iterateWhile и т.п.
  // Для следующих 2х строчек есть аналоги import tofu.syntax.raise._ + import tofu.syntax.handle._
  import cats.syntax.applicativeError._
  import cats.syntax.monadError._

  case class User(name: String)

  case class UserNotFound(id: String) extends Exception(s"User '$id' not found!")

  def log(message: String): EvalErr[Unit] =
    Eval.later(Right(println(message)))

  def getUserFromDatabase(id: Long): EvalErr[User] =
    Map(
      1L     -> User("Oleg"),
      234L   -> User("Vasiliy"),
      78237L -> User("Alexander")
    ).get(id) match {
      case Some(value) =>
        // Создать EvalErr[User], имея User
        value.pure
      case None =>
        // "Выбросить" ошибку
        new NoSuchElementException(id.toString).raiseError
    }

  def authenticateUser(id: Long): EvalErr[String] =
    getUserFromDatabase(id)
      // Исполнить side-эффект
      .flatTap(user => log(s"$user authenticated"))
      // То же, что и .map. Метод .map из синтаксиса не получится, т.к. определен метод .map у Eval
      .fmap(user => s"Welcome, ${user.name}!")
      // Заменить одни ошибки другими с помощью частичной функции
      .adaptError { case noSuchElement: NoSuchElementException => UserNotFound(noSuchElement.getMessage) }
      // Исполнить side-эффект при ошибке
      .onError { case UserNotFound(id) => log(s"User '$id' was not found!") }
      // Обработать ошибку, вернуть "нормальное" значение. Если функция-обработчик возвращает F[A],
      // то можно воспользоваться .handleErrorWith
      .handleError(_ => "I don't know you!")

  println(authenticateUser(1).value)
  println()
  println(authenticateUser(2).value)
}
