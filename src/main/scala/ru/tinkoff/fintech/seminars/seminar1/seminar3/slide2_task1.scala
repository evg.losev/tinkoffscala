package ru.tinkoff.fintech.seminars.seminar1.seminar3

import java.time.LocalDate

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNel
import cats.implicits._
import ru.tinkoff.fintech.Homeworks._

object slide2_task1 extends App {
  type Error = String
  type Rule = Passport => ValidatedNel[Error, Passport]

  def validate(rules: List[Rule],
               passports: List[Passport]): (List[Error], List[Passport]) = {
    val (results, errors) = passports
      .map(passport => rules.map(_ apply passport).sequence.map(_ => passport))
      .partition(_.isValid)

    (
      errors.sequence.fold(_.toList, _ => Nil),
      results.sequence.fold(_ => Nil, identity)
    )
  }

  val numberRule: Rule = passport =>
    if (passport.number == "4")
      passport.validNel
    else
      s"invalid number length: ${passport.number}".invalidNel

  val validUntilRule: Rule = passport =>
    if (passport.validUntil.isAfter(LocalDate.of(2023, 1, 1)))
      passport.validNel
    else s"invalid expiry date: ${passport.validUntil}".invalidNel

  val pass1 = Passport("1", LocalDate.of(2021, 1, 1))
  val pass2 = Passport("2", LocalDate.of(2022, 2, 2))
  val pass3 = Passport("3", LocalDate.of(2023, 3, 3))
  val pass4 = Passport("4", LocalDate.of(2024, 4, 4))

  val (errors, validPassports) =
    validate(List(numberRule, validUntilRule), List(pass1, pass2, pass3, pass4))

  println(errors)
  println(validPassports)

  assert(
    errors.toSet == Set("invalid number length: 1",
                        "invalid expiry date: 2021-01-01",
                        "invalid number length: 2",
                        "invalid expiry date: 2022-02-02",
                        "invalid number length: 3"))
  assert(validPassports == List(pass4))
}
