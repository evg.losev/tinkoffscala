package ru.tinkoff.fintech.seminars.seminar1.seminar2

import cats.{Eval, Monad}

import scala.annotation.tailrec
import scala.util.Random

object slide3_common extends App {

  sealed trait MyOption[+A]

  case class MySome[A](a: A) extends MyOption[A]

  case object MyNone extends MyOption[Nothing]

  // Определяем три метода, получаем готовые реализации для многих других
  implicit object myOptionMonad extends Monad[MyOption] {
    def flatMap[A, B](fa: MyOption[A])(f: A => MyOption[B]): MyOption[B] =
      fa match {
        case MySome(a) => f(a)
        case MyNone    => MyNone
      }

    def pure[A](x: A): MyOption[A] = MySome(x)

    @tailrec
    def tailRecM[A, B](a: A)(f: A => MyOption[Either[A, B]]): MyOption[B] = f(a) match {
      case MyNone              => MyNone
      case MySome(Left(nextA)) => tailRecM(nextA)(f)
      case MySome(Right(b))    => MySome(b)
    }
  }

  import cats.syntax.all._

  val someInt = 1.pure[MyOption]
  println(someInt)
  println(someInt.map(_ * 2))
  println(someInt.flatMap(x => MySome(x * 4)))
  println(someInt.flatMap(x => MyNone))

  println(Eval.later("Hello world").flatTap(x => Eval.later(println(x + "!!!"))).value)

  val randomInt = Eval.always(Random.nextInt(10))
  randomInt.flatTap(x => Eval.later(println(x))).iterateWhile(_ != 0).value
}
