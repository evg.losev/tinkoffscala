package ru.tinkoff.fintech.seminars.seminar1.seminar3

import cats.data.State

object slide3_state extends App {
  case class Stock(amount: Int, price: Double)
  type Account = Map[String, List[Stock]]
  type AccountAction[T] = State[Account, T] // S => (S, T)

  def getAllStocks: AccountAction[Account] =
    State.get[Account]

  def totalPrice: AccountAction[Double] =
    State.inspect(
      acc =>
        acc.values
          .flatMap(stocks => stocks.map(stock => stock.amount * stock.price))
          .sum)

  def buyStock(name: String, stock: Stock): AccountAction[Unit] =
    State.modify(acc => acc.updated(name, stock :: acc.getOrElse(name, Nil)))

  def sellAllStocks: AccountAction[Unit] =
    State.set(Map.empty)

  val operations = for {
    _ <- buyStock("Yandex", Stock(1, 2.0))
//    _ <- sellAllStocks
    _ <- buyStock("Tinkoff", Stock(5, 2.0))
    _ <- buyStock("Yandex", Stock(2, 1.0))
    _ <- buyStock("Tinkoff", Stock(10, 3.0))
  } yield ()

  val (account, price) = (for {
    _ <- operations
    price <- totalPrice
  } yield price).run(Map("Gazprom" -> List(Stock(1, 20.0)))).value

  println(account)
  println(price)

}
