package ru.tinkoff.fintech.seminars.seminar1.collection

object s3_creation extends App {

  val matrix = List.fill(2, 3)(0)
  println(matrix)

  val list1 = List(1, 2, 3, 4)
  val list2 = List(4, 5, 7, 8)

  println(list1 :+ 5)
  println(list1 ++ list2)

  println(list1.splitAt(2))

}

