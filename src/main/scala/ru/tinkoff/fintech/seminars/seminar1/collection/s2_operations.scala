package ru.tinkoff.fintech.seminars.seminar1.collection

object slide3_list_operations extends App {
  val list: List[Int] = List(0, 1, 2, 3, 4, 5, 6)

  println(list.take(4))

  println(list.drop(3))

  println(list.takeWhile(_ < 5))

  println(list.filter(_ % 2 == 0))




  val doubledList: List[Int] = list.map(_ * 2)
  println(doubledList)

  val Alphabet = "abcdefg"
  val charList: List[Char] = list.map(el => Alphabet(el))
  println(charList)

  val wordList = list.map{
    case 1 => "one"
    case 2 => "two"
    case _ => "oops"
  }
  println(wordList)


  val collectedList = list.collect{
    case 1 => "one"
    case 2 => "two"
  }
  println(collectedList)



  val expandedList1: List[List[Int]] = list.map(el => List.fill(el)(el))
  val flattenedList1: List[Int] = expandedList1.flatten

  val expandedList2 = list.flatMap(el => List.fill(el)(el))
  println(expandedList2)



  println(list.grouped(2).toList)
  println(list.groupBy(_ % 2))


  println(list.forall(_ < 10))
  println(list.exists(_ % 4 == 0))
}


