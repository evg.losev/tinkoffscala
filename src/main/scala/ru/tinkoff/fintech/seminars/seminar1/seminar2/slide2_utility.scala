package ru.tinkoff.fintech.seminars.seminar1.seminar2

import cats.{Eq, Show}

object slide2_show extends App {
  case class CreditCard(number: String, validUntil: String, cvv: String)

  implicit object showCreditCard extends Show[CreditCard] {
    def show(t: CreditCard): String = s"CreditCard{number='${"**** " * 3}${t.number.takeRight(4)}'}"
  }

  import cats.syntax.show._

  val creditCard = CreditCard("2537 9146 8989 9293", "8/19", "123")
  println(creditCard.show)

  case class User(login: String, password: String)
  val user = User("valentiay", "12345")
  // Не скомпилируется, потому что нет инстансов тайпкласса
  //  println(user.show)
  //  println(1.show)

  // Если добавить, инстанс тайпкласса, код заработает
  import cats.instances.int._
  println(1.show)
}

object slide2_eq extends App {
  case class UserId(value: String) extends AnyVal

  def handleUser(isRecognized: Boolean): Unit =
    if (isRecognized) {
      println("User recognized")
    } else {
      println("Not authorized!")
    }

  val userId = "admin"
  handleUser(userId == UserId("admin"))

  implicit object userIdEq extends Eq[UserId] {
    def eqv(x: UserId, y: UserId): Boolean = x.value == y.value
  }

  import cats.syntax.eq._

  // Не скомпилируется, т.к. разные типы
  // handleUser(userId === UserId("admin"))

  val correctUserId = UserId("admin")
  handleUser(correctUserId === UserId("admin"))

  println(correctUserId =!= UserId("admin"))
}