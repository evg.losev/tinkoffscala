package ru.tinkoff.fintech.seminars.seminar1.tryOptionEither

// Try - тип, представляющий результат вычисления, которое могло закончиться успешно или выбросить исключение
import scala.util.{Failure, Success, Try}

object s4_try_overview extends App {
  // два конструктора: успех и ошибка
  import scala.util.{Success, Failure}

  val success: Try[String] = Success("Hola!")
  val failure: Try[String] = Failure(new RuntimeException("Not today"))

  println(success)
  println(s"success: ${success.isSuccess}")

  println(failure)
  println(s"failure: ${failure.isFailure}")
}

object s4_try_example extends App {
  val emptyList = List.empty[Int]

  val head: Try[Int] = Try(emptyList.head) // не бросает ошибку!

  println(head)
}

object s4_try_recovering extends App {

  def getNumber(optNumber: Option[Int]): Int =
    Try(optNumber.get) match {
      case Success(x) => x
      case Failure(exc) =>
        println(s"There was an exception $exc")
        0
    }
  println(getNumber(Some(42)))
  println(getNumber(None))

  val emptyList = List.empty[Int]

  val number = Try(emptyList.head).getOrElse(0)
  println(number)

  val numberString: String =
    Try(emptyList.head).fold(e => e.toString, x => x.toString)
  println(numberString)

}

object s4_try_operations extends App {
  val list = List(0)
  val tryHead = Try(list.head)

  println(tryHead)

  println(tryHead.filter(_ > 0))

  println(tryHead.collect {
    case x if x % 2 == 1 => x.toString
  })

  println(tryHead.map(_ / 0))

  println(tryHead.flatMap(x => Success(list(x + 1))))
}

object s4_try_option_either extends App {
  val tryHead = Try(List.empty[Int].head)

  println(tryHead.toOption)

  println(tryHead.toEither)
}
