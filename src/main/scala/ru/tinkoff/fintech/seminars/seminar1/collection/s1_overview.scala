package ru.tinkoff.fintech.seminars.seminar1.collection

import scala.collection.mutable
import scala.collection.immutable
import scala.collection
import scala.collection.immutable.ArraySeq           // общие абстрактные типы

object slide2_buffer extends App {
  // аналог ArrayList из Java
  val buffer = mutable.Buffer(1, 2, 3)
  println(buffer(0))

  buffer += 4
  println(buffer)
}

object slide2_list extends App {
  // односвязный список
  val list1 = 1 :: 2 :: 3 :: Nil
  val list2 = Nil.::(3).::(2).::(1)
  println(list1 == list2)

  // эффективные O(1) операции

  val prependedList = 0 :: list1

  val head = prependedList.head // на пустом списке бросит исключение
  val tail = prependedList.tail // на пустом списке бросит исключение
  println(head)
  println(tail)
}

object slide2_vector extends App {
  // неизменяемый вектор
  // реализован с помощью bitmapped array trie
  val vector = Vector(1, 2, 3)

  val prependedVector = 0 +: vector
  val appendedVector  = vector :+ 4
  val elementAt       = vector(1)
  val modifiedVector  = vector.updated(1, 42)
  println(prependedVector)
  println(appendedVector)
  println(elementAt)
  println(modifiedVector)
}

object slide2_lazylist extends App {
  val lazyList: LazyList[Int] = 1 #:: 2 #:: LazyList()
  println(lazyList)

  val forcedList = lazyList.force
  println(forcedList)

  lazy val fibs: LazyList[Int] = 1 #:: 1 #:: fibs.zip(fibs.tail).map { case (a, b) => a + b }
  println(fibs.take(10).force)
}

object slide2_arrayseq extends App {
  val seq = immutable.ArraySeq(1, 2, 3)
  println(seq(2))
}


object slide2_set extends App {
  //  реализован с помощью HashSet
  val set1 = Set(1, 2, 3)
  val set2 = Set(3, 4, 5)
  println(set1(2))
  println(set1 + 42)
  println(set1 - 2)
  println(set1 ++ set2)
  println(set1 -- set2)
  println(set1.intersect(set2))
}

object slide2_map extends App {

  val pair: Tuple2[Int, String] = 1 -> "one"

  // ассоциативный массив
  val m = Map(1 -> "one", 2 -> "two")

  println(m(1))
  println(m + (3 -> "three"))
  println(m + (2 -> "three"))
  println(m.keys)
}
