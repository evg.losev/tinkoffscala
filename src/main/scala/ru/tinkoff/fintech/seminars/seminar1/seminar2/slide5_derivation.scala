package ru.tinkoff.fintech.seminars.seminar1.seminar2

import cats.Contravariant
import magnolia._

trait YamlEncoder[A] {
  def encode(a: A, margin: Int): String
}

object YamlEncoder {
  // Задаем тайпкласс, который будет выводиться
  type Typeclass[A] = YamlEncoder[A]

  // Определяем, как вывести тайпкласс для case-класса, имея тайпклассы для его параметров
  def combine[A](ctx: CaseClass[YamlEncoder, A]): YamlEncoder[A] =
    (value: A, margin: Int) =>
      ctx.parameters.map { p =>
        " " * margin +
          p.label + ":\n" +
          p.typeclass.encode(p.dereference(value), margin + 4)
      }.mkString("\n")

  // Определяем, как вывести тайпкласс для sealed-трейта
  def dispatch[A](ctx: SealedTrait[YamlEncoder, A]): YamlEncoder[A] =
    (value: A, margin: Int) => ctx.dispatch(value) { sub => sub.typeclass.encode(sub.cast(value), margin) }

  // Сам макрос, который выводит тайпклассы
  implicit def gen[A]: YamlEncoder[A] = macro Magnolia.gen[A]

  // Добавляет метод contramap и другие к YamlEncoder
  implicit val contravariant: Contravariant[YamlEncoder] =
    new Contravariant[YamlEncoder] {
      def contramap[A, B](fa: YamlEncoder[A])(f: B => A): YamlEncoder[B] =
        (b: B, margin: Int) => fa.encode(f(b), margin)
    }

  // Инстансы для базовых типов
  implicit val stringEncoder: YamlEncoder[String] =
    (a: String, margin: Int) => " " * margin + a

  import cats.syntax.contravariant._

  implicit val charEncoder: YamlEncoder[Char]     = stringEncoder.contramap(_.toString)
  implicit val intEncoder: YamlEncoder[Int]       = stringEncoder.contramap(_.toString)
  implicit val doubleEncoder: YamlEncoder[Double] = stringEncoder.contramap(_.toString)

  implicit def listEncoder[A](implicit aEncoder: YamlEncoder[A]): YamlEncoder[List[A]] =
    (as: List[A], margin: Int) =>
      as.flatMap(a =>
          aEncoder.encode(a, 0).linesIterator.splitAt(1) match {
            case (head, tail) =>
              head.map(line => " " * (margin - 4) + "  - " + line) ++
                tail.map(line => " " * margin + line)
          }
        )
        .mkString("\n")
}

object slide5_derivation extends App {
  case class Credentials(user: String, password: String)

  case class DatabaseConfig(hosts: List[String], credentials: Credentials)

  case class BackendConfig(url: String, credentials: Credentials)

  case class Limit(method: String, requests: Int, seconds: Int)

  case class Config(database: DatabaseConfig, backend: BackendConfig, limits: List[Limit])

  val cfg = Config(
    DatabaseConfig(
      List("database-1.host.domain", "database-2.host.domain", "database-3.host.domain"),
      Credentials("dbuser", "dbpasswd")
    ),
    BackendConfig("backend.host.domain", Credentials("beuser", "bepassword")),
    List(
      Limit("authentificate", 10, 1),
      Limit("authentificate", 50, 60)
    )
  )

  println(YamlEncoder.gen[Config].encode(cfg, 0))
}
