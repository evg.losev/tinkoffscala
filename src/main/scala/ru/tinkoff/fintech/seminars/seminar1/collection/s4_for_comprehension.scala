package ru.tinkoff.fintech.seminars.seminar1.collection

object s4_for_comprehension extends App {
  val ints = List(1, 2, 3, 4)
  val chars = List('a', 'b', 'c')

  val pairs1 =
    for {
      int <- ints
      char <- chars
    } yield (int, char)

  val pairs2 =
    ints.flatMap( int =>
      chars.map( char =>
        (int, char)
      )
    )

  println(pairs1)
  println(pairs2)

  println(pairs1 == pairs2)


  val xs = List(5, 4, 9, 1, 2)
  val ys = List(8, 3, 5, 0, 4, 6)
  val zs = List(15, 0, 12)

  val numbers1 =
    for {
      x <- xs
      y <- ys
      z <- zs if z == x * y
    } yield (x, y)

  val numbers2 =
    xs.flatMap( x =>
      ys.flatMap(y =>
        zs.withFilter(z => z == x * y).map(z => (x, y))
      )
    )

  println(numbers1)
  println(numbers2)
  println(numbers1 == numbers2)


  val intsTo = for {
    i <- 1 to 10
  } yield i * 2

  println(intsTo)

  val intsUntil = for {
    i <- 1 until 10
  } yield i * 2

  println(intsUntil)


}
