package ru.tinkoff.fintech.seminars.seminar1.seminar3

import cats.data.{Chain, State}
import cats.implicits._

import ru.tinkoff.fintech.Homeworks._

object slide4_task2 extends App {
  sealed trait ParserEvent
  case object Opened extends ParserEvent
  case object Closed extends ParserEvent

  case class ParserError(message: String, pos: Int)
  case class ParserState(balance: Int = 0,
                         curPos: Int = 0,
                         failed: Option[ParserError] = None) {
    def incBalance: ParserState = copy(balance = balance + 1)
    def decBalance: ParserState = copy(balance = balance - 1)
    def incPos: ParserState = copy(curPos = curPos + 1)
    def failed(error: ParserError): ParserState = copy(failed = Some(error))
  }

  type ParserResult = Either[ParserError, Chain[ParserEvent]]
  type Parse[A] = State[ParserState, A]

  def parse(data: String): Parse[ParserResult] =
    data.foldLeft(State.empty[ParserState, ParserResult]) { (state, char) =>
      {
        state.transform { (s, result) =>
          s.failed.map(error => (s, Left(error))).getOrElse {
            char match {
              case '{' =>
                (s.incBalance.incPos, result.map(_.append(Opened)))
              case _ =>
                if (s.balance > 0)
                  (s.decBalance.incPos, result.map(_.append(Closed)))
                else {
                  val error =
                    ParserError("unmatched closing parenthesis", s.curPos + 1)
                  (s.failed(error), error.asLeft[Chain[ParserEvent]])
                }
            }
          }
        }
      }
    }

  val piece1 = "{{}"
  val piece2 = "}}"
  val piece3 = "{}"

  val (res1, res2, res3) = (for {
    res1 <- parse(piece1)
    res2 <- parse(piece2)
    res3 <- parse(piece3)
  } yield (res1, res2, res3)).runA(ParserState()).value

  println(res1)
  println(res2)
  println(res3)

  assert(res1 == Right(Chain(Opened, Opened, Closed)))
  assert(res2 == Left(ParserError("unmatched closing parenthesis", 5)))
  assert(res3 == Left(ParserError("unmatched closing parenthesis", 5)))

}
