package ru.tinkoff.fintech.seminars.seminar1.collection

object s5_fold extends App {
  val ints: List[Int] = List(1, 2, 3, 4, 5)

  val reducedInts = ints.reduce((a, b) => 10 * a + b)
  println(reducedInts)

  val foldedInts = ints.foldLeft(0)((acc, el) => acc * 10 + el)
  println(foldedInts)
}
