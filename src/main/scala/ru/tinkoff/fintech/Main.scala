package ru.tinkoff.fintech

import ru.tinkoff.fintech.homeworks.homework1.task2_caesar.indexedUpper
import zio.ZIO
import zio.console.putStrLn

object Main extends zio.App {
  def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    for {
      _ <- putStrLn("Hello, Fintech School!")
    } yield 0

  println()
  "word".getBytes.foreach(b => print(b + " "))
  println("word".getBytes.map(byteValue => byteValue + 2).map(_.toChar).mkString)
  var encrypted = "SCALAPENIO".getBytes.map(byteValue => byteValue + 242).map(_.toChar).mkString
  println(encrypted)
  var decrypted = encrypted.getBytes.map(byteValue => byteValue - 242).map(_.toChar).mkString
  println(decrypted)
  encrypted = "XYZ".getBytes.map(byteValue => byteValue + 3).map(_.toChar).mkString
  println(encrypted)
  decrypted = encrypted.getBytes.map(byteValue => byteValue - 3).map(_.toChar).mkString
  println(decrypted)
  val upper =
    List("A","B","C","D","E","F","G","H","I","J","K","L","M","N", "O","P","Q",
      "R","S","T","U","V","W","X","Y","Z")
  val indexedUpper = upper.zipWithIndex.toMap.map(entry => (entry._1, entry._2 + 1))
  println(indexedUpper)
  println((upper.indices zip upper).toMap)
  val swapIndexedUpper = indexedUpper.map(_.swap)
  println("swapIndexedUpper" + swapIndexedUpper)

  //18-0-18
  //16-0-16
//  println(3-(3/26*26))
//  println(3-(3/25*25))


//  println("18, 242 : " + getValue(18, 242))
  println("____________")


//  encrypted = "SCALAPENIO".map(string => getValue(indexedUpper(string.toString), 242)).map(number => swapIndexedUpper(number)).mkString
//  println("SCALAPENIO encrypted :: " + encrypted)
//
//  decrypted = encrypted.map(string => getDecriptedValue(indexedUpper(string.toString), 242)).map(number => swapIndexedUpper(number)).mkString
//  println("SCALAPENIO DECRIPTED  :: " + decrypted)
//
//  encrypted = "SCALA".map(string => getValue(indexedUpper(string.toString), 2)).map(number => swapIndexedUpper(number)).mkString
//  println("SCALA encrypted :: " + encrypted)
//
//  decrypted = encrypted.map(string => getDecriptedValue(indexedUpper(string.toString), 2)).map(number => swapIndexedUpper(number)).mkString
//  println("SCALA DECRIPTED  :: " + decrypted)

  encrypted = "XYZ".map(string => getValue(indexedUpper(string.toString), 3)).map(number => swapIndexedUpper(number)).mkString
  println("XYZ encrypted :: " + encrypted)
  //  println("SCALAPENIO decrv :: " + getDecriptedValue(10, 242))

  decrypted = encrypted.map(string => getDecriptedValue(indexedUpper(string.toString), 3)).map(number => swapIndexedUpper(number)).mkString
  println("SCALA DECRIPTED  :: " + decrypted)
//  println(getValue(18, 242))
//  println((242%26).toChar)
//  println(indexedUpper.size-1)
//  def getValue(int: Int): Int = if (int < indexedUpper.size - 1) int else int%(indexedUpper.size-1)

  def getValue(strindex: Int, offset: Int): Int = {
//    println("offset%26 :: " + offset%26)
    if (offset > 26 && (strindex + (offset%26)) > 26){
//      println("var1 ::  " + (strindex + (offset%26))%26)
      (strindex + (offset%26))%26
    } else if (offset > 26 && (strindex + (offset%26)) <= 26) {
//      println("var2 ::  " + (strindex + (offset%26)))
      strindex + (offset%26)
    }else if (offset < 26 && (strindex + (offset%26)) > 26) {
      println("var3 ::  " + (strindex + (offset%26)))
      strindex + offset - 26
    } else strindex + offset
  }
  def getDecriptedValue(strindex: Int, offset: Int): Int =
    if (offset > 26) {
      val x = offset-offset/26*26
      println("x :: " + x)
      if (strindex - x < 1){
        println("value1 :: " + ( strindex - x + 26))
        strindex - x + 26 }
      else strindex - x
    } else {
      if (strindex - offset < 1){ strindex - offset + 26 }
      else strindex - offset
    }
//  def getDecriptedValue(strindex: Int, offset: Int): Int =
//    if ((strindex-offset) < indexedUpper.size - 1 && (strindex-offset) >= 0) strindex-offset
//    else {
//      println(offset%(indexedUpper.size-1))
//      println(strindex)
//      offset-(offset/(indexedUpper.size-1)*25)}

//  println(260%25) //10 = К
//  println("decription " + ((242/25)+10-1))
//  println("decription " + getDecriptedValue(19, 242))
}